import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from '../app/app.component';
import { AppModule } from '../app/app.module';

@NgModule({
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		AppModule,
	],
	bootstrap: [AppComponent],
	providers: [],
})
export class AppBrowserModule {
}
