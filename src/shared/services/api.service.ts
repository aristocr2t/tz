import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import environment from '../../environment';

const API_HOST = environment.browserApiUrl;

@Injectable({ providedIn: 'root' })
export class ApiService {
	constructor(private readonly $http: HttpClient) {}

	search(params: SearchParams): Observable<ListBody<Question>> {
		if (!params.page) {
			params.page = 1;
		}

		if (!params.pagesize) {
			params.pagesize = 10;
		}

		if (!params.order) {
			params.order = 'desc';
		}

		if (!params.sort) {
			params.sort = 'activity';
		}

		if (!params.site) {
			params.site = 'stackoverflow';
		}

		return this.$http
			.get<ListBody<Question>>(`${API_HOST}/2.2/search/advanced`, { params: this.parseParams(params) });
	}

	getQuestion(id: number, params: {
		page?: number;
		pagesize?: number;
		site?: 'stackoverflow';
	} = {}): Observable<Question | undefined> {
		if (!params.site) {
			params.site = 'stackoverflow';
		}

		return this.$http
			.get<ListBody<Question>>(`${API_HOST}/2.2/questions/${id}`, { params: this.parseParams(params) })
			.pipe(map(resBody => resBody.items.first()));
	}

	getQuestionAnswers(id: number, params: AnswersParams): Observable<ListBody<Answer>> {
		if (!params.page) {
			params.page = 1;
		}

		if (!params.pagesize) {
			params.pagesize = 10;
		}

		if (!params.order) {
			params.order = 'desc';
		}

		if (!params.sort) {
			params.sort = 'creation';
		}

		if (!params.site) {
			params.site = 'stackoverflow';
		}

		return this.$http
			.get<ListBody<Answer>>(`${API_HOST}/2.2/questions/${id}/answers`, { params: this.parseParams(params) });
	}

	protected parseParams(params: { [param: string]: any }): { [param: string]: string } {
		const parsedParams: { [param: string]: string } = {};
		const entries = Object.entries(params);

		for (const [key, value] of entries) {
			const parsedValue = this.parseParamValue(value);

			if (parsedValue !== undefined) {
				parsedParams[key] = parsedValue;
			}
		}

		return parsedParams;
	}

	protected parseParamValue(value: number | string | boolean | Date | undefined): string | undefined {
		switch (typeof value) {
			case 'string':
				return value;
			case 'number':
				return value.toString();
			case 'boolean':
				return value ? 'True' : 'False';
			case 'object':
				if (value instanceof Date) {
					return value.getTime().toString();
				}

				return undefined;
			default:
				return undefined;
		}
	}
}

export interface SearchParams {
	page?: number;
	pagesize?: number;
	order?: 'asc' | 'desc';
	sort?: 'activity' | 'votes' | 'creation' | 'relevance';
	site?: 'stackoverflow';
	q?: string;
	user?: number;
	views?: number;
	answers?: number;
	fromdate?: number;
	todate?: number;
	min?: number;
	max?: number;
	body?: string;
	nottagged?: string;
	tagged?: string;
	title?: string;
	url?: string;
	accepted?: boolean;
	closed?: boolean;
	migrated?: boolean;
	notice?: boolean;
	wiki?: boolean;
}

export interface AnswersParams {
	page?: number;
	pagesize?: number;
	order?: 'asc' | 'desc';
	sort?: 'activity' | 'votes' | 'creation' | 'relevance';
	site?: 'stackoverflow';
	fromdate?: number;
	todate?: number;
	min?: number;
	max?: number;
}

export interface Answer {
	owner: {
		reputation: number;
		user_id: number;
		user_type: string;
		accept_rate: number;
		profile_image: string;
		display_name: string;
		link: string;
	};
	is_accepted: boolean;
	score: number;
	last_activity_date: number;
	last_edit_date: number;
	creation_date: number;
	answer_id: number;
	question_id: number;
	content_license: string;
}

export interface Question {
	tags: string[];
	owner: {
		reputation: number;
		user_id: number;
		user_type: string;
		profile_image: string;
		display_name: string;
		link: string;
	};
	is_answered: boolean;
	view_count: number;
	answer_count: number;
	score: number;
	last_activity_date: number;
	creation_date: number;
	question_id: number;
	content_license: string;
	link: string;
	title: string;
}

export interface ListBody<T> {
	items: T[];
	has_more: boolean;
	quota_max: number;
	quota_remaining: number;
}
