import { DOCUMENT } from '@angular/common';
import { Inject, Injectable, Optional, PLATFORM_ID } from '@angular/core';

@Injectable({
	providedIn: 'root',
})
export class CookieService {
	private readonly isBrowser: boolean;

	constructor(
		@Optional() @Inject(DOCUMENT) private readonly document: Document,
		@Optional() @Inject('REQUEST') private readonly $req: { headers: { cookie: string | undefined } },
		@Inject(PLATFORM_ID) platformId: PlatformIds,
	) {
		this.isBrowser = platformId === 'browser';
	}

	/**
	 * @param name Cookie name
	 * @returns boolean - whether cookie with specified name exists
	 */
	check(name: string): boolean {
		name = encodeURIComponent(name);

		const regExp: RegExp = this.getCookieRegExp(name);
		const exists: boolean = regExp.test(this.getCookieRaw());

		return exists;
	}

	/**
	 * @param name Cookie name
	 * @returns property value
	 */
	get(name: string): string {
		if (this.check(name)) {
			name = encodeURIComponent(name);

			const regExp: RegExp = this.getCookieRegExp(name);
			const result: RegExpExecArray = regExp.exec(this.getCookieRaw()) as RegExpExecArray;

			try {
				return decodeURIComponent(result[1]);
			} catch (err) {
				// the cookie probably is not uri encoded. return as is
				return result[1];
			}
		} else {
			return '';
		}
	}

	/**
	 * @returns all the cookies in json
	 */
	getAll(): Record<string, string> {
		const cookies: Record<string, string> = {};
		const cookie: string = this.getCookieRaw();

		if (cookie && cookie !== '') {
			cookie.split(';').forEach(currentCookie => {
				const [cookieName, cookieValue] = currentCookie.split('=');
				cookies[decodeURIComponent(cookieName.replace(/^ /, ''))] = decodeURIComponent(cookieValue);
			});
		}

		return cookies;
	}

	/**
	 * @param name     Cookie name
	 * @param value    Cookie value
	 * @param expires  Number of days until the cookies expires or an actual `Date`
	 * @param path     Cookie path
	 * @param domain   Cookie domain
	 * @param secure   Secure flag
	 * @param sameSite OWASP samesite token `Lax`, `None`, or `Strict`. Defaults to `Lax`
	 */
	set(
		name: string,
		value: string,
		expires?: number | Date,
		path?: string,
		domain?: string,
		secure?: boolean,
		sameSite: 'Lax' | 'None' | 'Strict' = 'Lax',
	): void {
		if (!this.isBrowser) {
			return;
		}

		let cookieString: string = `${encodeURIComponent(name)}=${encodeURIComponent(value)};`;

		if (expires) {
			if (typeof expires === 'number') {
				const dateExpires: Date = new Date(new Date().getTime() + (expires * 1000 * 60 * 60 * 24));

				cookieString += `expires=${dateExpires.toUTCString()};`;
			} else {
				cookieString += `expires=${expires.toUTCString()};`;
			}
		}

		cookieString += `path=${path ?? '/'};`;

		if (domain) {
			cookieString += `domain=${domain};`;
		}

		if (secure === false && sameSite === 'None') {
			secure = true;
			console.warn(`Cookie ${name} was forced with secure flag because sameSite=None.`
					+ `More details : https://github.com/stevermeister/ngx-cookie-service/issues/86#issuecomment-597720130`);
		}

		if (secure) {
			cookieString += 'secure;';
		}

		cookieString += `sameSite=${sameSite};`;

		this.document.cookie = cookieString;
	}

	/**
	 * @param name   Cookie name
	 * @param path   Cookie path
	 * @param domain Cookie domain
	 */
	delete(name: string, path?: string, domain?: string, secure?: boolean, sameSite: 'Lax' | 'None' | 'Strict' = 'Lax'): void {
		if (!this.isBrowser) {
			return;
		}

		this.set(name, '', new Date('Thu, 01 Jan 1970 00:00:01 GMT'), path, domain, secure, sameSite);
	}

	/**
	 * @param path   Cookie path
	 * @param domain Cookie domain
	 */
	deleteAll(path?: string, domain?: string, secure?: boolean, sameSite: 'Lax' | 'None' | 'Strict' = 'Lax'): void {
		if (!this.isBrowser) {
			return;
		}

		const cookies = this.getAll();

		for (const cookieName in cookies) {
			if (cookies.hasOwnProperty(cookieName)) {
				this.delete(cookieName, path, domain, secure, sameSite);
			}
		}
	}

	private getCookieRaw(): string {
		return this.isBrowser ? this.document.cookie : (this.$req.headers.cookie ?? '');
	}

	/**
	 * @param name Cookie name
	 * @returns property RegExp
	 */
	private getCookieRegExp(name: string): RegExp {
		const escapedName: string = name.replace(/([\[\]\{\}\(\)\|\=\;\+\?\,\.\*\^\$])/gi, '\\$1');

		return new RegExp(`(?:^${escapedName}|;\\s*${escapedName})=(.*?)(?:;|$)`, 'g');
	}
}
