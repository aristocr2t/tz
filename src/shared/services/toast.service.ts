import { Overlay } from '@angular/cdk/overlay';
import { ComponentPortal, PortalInjector } from '@angular/cdk/portal';
import { Injectable, Injector } from '@angular/core';

import { ToastComponent, ToastData, ToastRef } from '../components/toast/toast.component';

@Injectable({
	providedIn: 'root',
})
export class ToastService {
	constructor(private readonly overlay: Overlay, private readonly parentInjector: Injector) {}

	showToast(data: Partial<ToastData>): ToastRef {
		const toastRef = new ToastRef(this.overlay);

		this.attachToastPortal(data, toastRef);

		return toastRef;
	}

	attachToastPortal(data: Partial<ToastData>, toastRef: ToastRef): void {
		const tokens = new WeakMap();

		if (!data.type) {
			data.type = 'info';
		}

		data.created = Date.now();

		tokens.set(ToastData, data);
		tokens.set(ToastRef, toastRef);

		const portalInjector = new PortalInjector(this.parentInjector, tokens);

		const portal = new ComponentPortal<ToastComponent>(ToastComponent, null, portalInjector);

		toastRef.overlayRef.attach(portal);
	}
}
