import { GlobalPositionStrategy, Overlay, OverlayRef } from '@angular/cdk/overlay';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';

@Component({
	selector: 'div#toast',
	templateUrl: './toast.component.html',
})
export class ToastComponent implements OnInit, OnDestroy {
	readonly types = {
		info: 'alert-circle-outline',
		warn: 'alert-rhombus-outline',
		error: 'alert-octagon-outline',
	};

	private timeoutId: number | undefined;

	created: string = '';

	constructor(readonly data: ToastData, readonly ref: ToastRef) {}

	ngOnInit(): void {
		if (this.data.timeout) {
			this.timeoutId = setTimeout(() => {
				this.close();
			}, this.data.timeout);
		} else {
			this.parseTime();
			this.timeoutId = setInterval(() => {
				this.parseTime();
			}, 60000);
		}
	}

	ngOnDestroy(): void {
		clearTimeout(this.timeoutId);
	}

	close(): void {
		this.ref.close();
	}

	parseTime(): void {
		let diff = Math.floor((Date.now() - this.data.created) / 60000);
		const divs = [60, 60, 24],
			divNames = [['минут', 'минуты', 'минуту'], ['часов', 'часа', 'час'], ['дней', 'дня', 'день']];

		let i = 0;

		while (i < divs.length && diff > divs[i]) {
			diff = Math.floor(diff / divs[i]);
			i++;
		}

		this.created = diff ? `${diff} ${this.getName(diff, divNames[i])} назад` : `только что`;
	}

	getName(v: number, names: string[]): string {
		const m = v % 10;
		const mm = v % 100;

		if (m === 0 || (m >= 5 && m <= 9) || (mm >= 11 && mm <= 14)) {
			return names[0];
		}

		if ((m >= 2 && m <= 4)) {
			return names[1];
		}

		return names[2];
	}
}

const GUTTER_WIDTH = 20;

export class ToastRef {
	private static readonly $$close: Subject<void> = new Subject<void>();
	private static readonly close: Observable<void> = ToastRef.$$close.asObservable();
	private static readonly openedRefs: ToastRef[] = [];
	private readonly closeSubscription: Subscription | undefined;
	private readonly positionStrategy: GlobalPositionStrategy;

	readonly overlayRef: OverlayRef;

	constructor(readonly overlay: Overlay) {
		this.positionStrategy = this.overlay
			.position()
			.global()
			.bottom(`${GUTTER_WIDTH}px`)
			.right(`${GUTTER_WIDTH}px`);

		this.overlayRef = this.overlay.create({ positionStrategy: this.positionStrategy });

		setTimeout(() => {
			this.updatePosition();
		});

		ToastRef.openedRefs.push(this);

		this.closeSubscription = ToastRef.close
			.subscribe(() => {
				this.updatePosition();
			});
	}

	close(): void {
		this.overlayRef.dispose();

		const index = ToastRef.openedRefs.indexOf(this);

		if (index > -1) {
			ToastRef.openedRefs.splice(index, 1);
		}

		ToastRef.$$close.next();

		if (this.closeSubscription && !this.closeSubscription.closed) {
			this.closeSubscription.unsubscribe();
		}
	}

	updatePosition(): void {
		const refs = ToastRef.openedRefs;
		let index = refs.indexOf(this);

		if (index === -1) {
			index = refs.length;
		}

		let h = 0;

		for (let i = 0; i < index; i++) {
			const height = refs[i].overlayRef.overlayElement.clientHeight;
			h += height + GUTTER_WIDTH;
		}

		this.positionStrategy.bottom(`${h + GUTTER_WIDTH}px`).right(`${GUTTER_WIDTH}px`);

		this.overlayRef.updatePosition();
	}
}

export class ToastData {
	created!: number;
	timeout!: number | undefined;
	type!: ToastType;
	title!: string;
	text!: string;
}

export type ToastType = 'warn' | 'error' | 'info';
