import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

export class PageEvent {
	hasMore!: boolean;
	page!: number;
	pageSize!: number;
	prevPage!: number;
}

@Component({
	selector: 'div#pagination',
	templateUrl: './pagination.component.html',
	styleUrls: ['./pagination.component.scss'],
})
export class PaginationComponent implements OnInit {
	nextCount!: number;

	$page!: number;
	$pageSize!: number;

	@Input()
	set page(value: string | number | undefined) {
		this.$page = isFinite(value) ? +value! : 1;
		this.nextCount = this.$page * this.$pageSize;
	}

	@Input()
	set pageSize(value: string | number | undefined) {
		this.$pageSize = isFinite(value) ? +value! : 10;
		this.nextCount = this.$page * this.$pageSize;
	}

	@Input()
	hasMore!: boolean;

	@Input()
	pageSizeOptions: number[] = [5, 10, 20, 50];

	@Output()
	change: EventEmitter<PageEvent> = new EventEmitter();

	prevPage!: number;

	ngOnInit(): void {
		this.prevPage = this.$page;
	}

	onNextPage(): void {
		if (this.hasMore) {
			return;
		}

		this.$page += 1;
		this.emitEvent();
		this.prevPage = this.$page;
	}

	onPrevPage(): void {
		if (this.$page <= 0) {
			return;
		}

		this.$page -= 1;
		this.emitEvent();
		this.prevPage = this.$page;
	}

	emitEvent(): void {
		const event = new PageEvent();
		event.hasMore = this.hasMore;
		event.page = this.$page;
		event.pageSize = this.$pageSize;
		event.prevPage = this.prevPage;
		this.change.emit(event);
	}
}
