import { OverlayModule } from '@angular/cdk/overlay';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PaginationComponent } from './components/pagination/pagination.component';
import { ToastComponent } from './components/toast/toast.component';
import { CheckboxDirective } from './directives/checkbox.directive';
import { ApiService } from './services/api.service';
import { CookieService } from './services/cookie.service';
import { ToastService } from './services/toast.service';

@NgModule({
	imports: [
		//
		CommonModule,
		RouterModule,
		HttpClientModule,
		FormsModule,
		ReactiveFormsModule,
		NgbModule,
		OverlayModule,
	],
	exports: [
		// Modules
		CommonModule,
		RouterModule,
		HttpClientModule,
		FormsModule,
		ReactiveFormsModule,
		NgbModule,
		// Directives
		CheckboxDirective,
		// Components
		PaginationComponent,
		ToastComponent,
	],
	declarations: [
		// Directives
		CheckboxDirective,
		// Components
		PaginationComponent,
		ToastComponent,
	],
	providers: [
		// Services
		ApiService,
		CookieService,
		ToastService,
	],
})
export class SharedModule {
}
