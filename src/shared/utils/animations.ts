import { animate, style, transition, trigger } from '@angular/animations';

export const FadeInAnimation = trigger('fadeIn', [
	transition('void => *', [
		style({
			opacity: 0,
			transform: 'translateY(15px)',
		}),
		animate(
			'300ms ease-in-out',
			style({
				opacity: 1,
				transform: 'translateY(0)',
			}),
		),
	]),
]);
