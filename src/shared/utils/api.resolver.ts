import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { ApiService } from '../services/api.service';

@Injectable({ providedIn: 'root' })
export class ApiResolver implements Resolve<any> {
	constructor(private readonly $api: ApiService) {}

	resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
		const component = route.component as unknown as ResolveComponent;

		if (typeof component !== 'function' || !component.__resolver) {
			throw new TypeError(`Can\`t get resolve options of ${component.name}`);
		}

		return component.__resolver(this.$api, route, state);
	}
}

export function Resolver(handler: ResolveHandler): ClassDecorator {
	return (target) => {
		(target as unknown as ResolveComponent).__resolver = handler;
	};
}

export type ResolveHandler = (api: ApiService, route: ActivatedRouteSnapshot, state: RouterStateSnapshot) => Observable<any>;
export type ResolveComponent = (new (...args: any[]) => any) & { __resolver: ResolveHandler };
