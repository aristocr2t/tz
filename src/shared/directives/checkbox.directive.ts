import { Directive, HostBinding, HostListener, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Directive({
	selector: '[checkbox]',
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			multi: true,
			// eslint-disable-next-line @typescript-eslint/no-use-before-define
			useExisting: forwardRef(() => CheckboxDirective),
		},
	],
})
export class CheckboxDirective implements ControlValueAccessor {
	@Input()
	disabled: boolean = false;

	@HostBinding('class.checked')
	value: boolean = false;

	onChange = (value: boolean): any => {};
	onTouched = (value: boolean): any => {};
	onValidatorChange = (): any => {};

	@HostListener('click')
	onClick(): void {
		this.writeValue(!this.value);
	}

	writeValue(value: boolean): void {
		if (this.disabled) {
			return;
		}

		this.value = value;
		this.onChange(this.value);
		this.onValidatorChange();
	}

	registerOnChange(fn: (value: boolean) => void): void {
		this.onChange = fn;
	}

	registerOnTouched(fn: (value: boolean) => void): void {
		this.onTouched = fn;
	}

	registerOnValidatorChange?(fn: () => void): void {
		this.onValidatorChange = fn;
	}

	setDisabledState?(disabled: boolean): void {
		this.disabled = disabled;
	}
}
