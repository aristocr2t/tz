Object.defineProperties(Array.prototype, {
	first: {
		value<T>(): T | undefined {
			return this[0] as T | undefined;
		},
		enumerable: false,
		writable: false,
	},
	last: {
		value<T>(): T | undefined {
			return this[this.length - 1] as T | undefined;
		},
		enumerable: false,
		writable: false,
	},
	equals: {
		value(array: any[]): boolean {
			if (array instanceof Array && this.length === array.length) {
				for (let i = 0, l = this.length; i < l; i++) {
					if (this[i] === array[i] || (this[i] instanceof Array && array[i] instanceof Array && (this[i] as any[]).equals(array[i]))) {
						return true;
					}
				}
			}

			return false;
		},
		enumerable: false,
		writable: false,
	},
});

const MathRound = Math.round;
const MathCeil = Math.ceil;
const MathFloor = Math.floor;
const $180pi = 180 / Math.PI;
const $pi180 = Math.PI / 180;

Object.defineProperties(Math, {
	round: {
		value(x: number, digits: number = 0): number {
			if (digits > 0) {
				digits = 10 ** MathRound.call(this, digits);

				return MathRound.call(this, x * digits) / digits;
			}

			return MathRound.call(this, x) as number;
		},
		enumerable: false,
		writable: false,
	},
	ceil: {
		value(x: number, digits: number = 0): number {
			if (digits > 0) {
				digits = 10 ** MathRound.call(this, digits);

				return MathCeil.call(this, x * digits) / digits;
			}

			return MathCeil.call(this, x) as number;
		},
		enumerable: false,
		writable: false,
	},
	floor: {
		value(x: number, digits: number = 0): number {
			if (digits > 0) {
				digits = 10 ** MathRound.call(this, digits);

				return MathFloor.call(this, x * digits) / digits;
			}

			return MathFloor.call(this, x) as number;
		},
		enumerable: false,
		writable: false,
	},
	toDegrees: {
		value(alpha: number): number {
			return alpha * $180pi;
		},
		enumerable: false,
		writable: false,
	},
	toRadians: {
		value(alpha: number): number {
			return alpha * $pi180;
		},
		enumerable: false,
		writable: false,
	},
});

export {};
