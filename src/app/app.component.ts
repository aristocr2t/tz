import { Component, OnInit } from '@angular/core';
import { Event, NavigationEnd, Router } from '@angular/router';
import { delay, filter, take } from 'rxjs/operators';

@Component({
	selector: 'div#root',
	templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
	constructor(private readonly $router: Router) {
		this.loadingOverlay();
	}

	ngOnInit(): void {
	}

	private loadingOverlay(): void {
		this.$router.events
			.pipe(
				filter((e: Event): e is NavigationEnd => e instanceof NavigationEnd),
				take(1),
				delay(100),
			)
			.subscribe(e => {
				document.getElementById('loading-overlay')?.classList.add('hide');
			});
	}
}
