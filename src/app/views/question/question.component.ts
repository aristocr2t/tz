import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription, forkJoin } from 'rxjs';

import { Answer, AnswersParams, FadeInAnimation, ListBody, PageEvent, Question, Resolver } from '../../../shared';

@Resolver((api, route) => {
	const { id } = route.params;
	const params = { ...route.queryParams } as AnswersParams;

	return forkJoin({
		question: api.getQuestion(id),
		answers: api.getQuestionAnswers(id, params),
	});
})
@Component({
	selector: 'div#question',
	templateUrl: './question.component.html',
	styleUrls: ['./question.component.scss'],
	animations: [FadeInAnimation],
})
export class QuestionComponent implements OnInit, OnDestroy {
	private readonly dataSubscription: Subscription;

	question!: Question;
	answers!: ListBody<Answer>;

	queryParams: AnswersParams = {};

	constructor(private readonly $router: Router, private readonly $route: ActivatedRoute) {
		this.dataSubscription = $route.data.subscribe(data => {
			const { question, answers } = data.payload;
			this.question = question;
			this.answers = answers;
			this.queryParams = { ...$route.snapshot.queryParams };
		});
	}

	search = (reload: boolean = true): void => {
		if (reload) {
			this.queryParams.page = undefined;
		}

		this.$router.navigate(['.'], {
			relativeTo: this.$route,
			queryParams: this.queryParams,
			queryParamsHandling: 'merge',
		}).catch(err => {
			console.error(err);
		});
	};

	ngOnInit(): void {
	}

	ngOnDestroy(): void {
		if (this.dataSubscription.closed) {
			this.dataSubscription.unsubscribe();
		}
	}

	onPageChange = (event: PageEvent): void => {
		this.queryParams.page = event.page;
		this.queryParams.pagesize = event.pageSize;
		this.search(false);
	};

	tsToDate = (ts: number): Date => new Date(ts * 1000);
}
