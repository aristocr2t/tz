import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { ApiResolver } from '../../shared';
import { SharedModule } from '../../shared/shared.module';
import { QuestionComponent } from './question/question.component';
import { SearchResultComponent } from './search-result/search-result.component';
import { SearchComponent } from './search/search.component';

const routes: Routes = [
	{
		path: '',
		redirectTo: '/search',
		pathMatch: 'full',
	},
	{
		path: 'search',
		component: SearchComponent,
	},
	{
		path: 'search_results',
		component: SearchResultComponent,
		runGuardsAndResolvers: 'paramsOrQueryParamsChange',
		resolve: {
			payload: ApiResolver,
		},
	},
	{
		path: 'question/:id',
		component: QuestionComponent,
		runGuardsAndResolvers: 'paramsChange',
		resolve: {
			payload: ApiResolver,
		},
	},
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, {
			onSameUrlNavigation: 'reload',
			preloadingStrategy: PreloadAllModules,
		}),
		SharedModule,
	],
	exports: [RouterModule],
	declarations: [
		QuestionComponent,
		SearchComponent,
		SearchResultComponent,
	],
})
export class ViewsModule {
}
