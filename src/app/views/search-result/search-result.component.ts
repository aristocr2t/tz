import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { ApiService, FadeInAnimation, ListBody, Question, Resolver, SearchParams } from '../../../shared';

@Resolver((api, route) => {
	const params = { ...route.queryParams } as SearchParams;

	return api.search(params);
})
@Component({
	selector: 'div#search-result',
	templateUrl: './search-result.component.html',
	styleUrls: ['./search-result.component.scss'],
	animations: [FadeInAnimation],
})
export class SearchResultComponent implements OnInit, OnDestroy {
	private readonly dataSubscription: Subscription;

	list!: ListBody<Question>;
	queryParams: SearchParams = {};

	similarList: ListBody<Question> | undefined;

	constructor(private readonly $router: Router, private readonly $route: ActivatedRoute, private readonly $api: ApiService) {
		this.dataSubscription = $route.data.subscribe(data => {
			this.list = data.payload;
			this.queryParams = { ...$route.snapshot.queryParams };
			this.similarList = undefined;
		});
	}

	search = (reload: boolean = true): void => {
		if (reload) {
			this.queryParams.page = undefined;
		}

		this.$router.navigate(['.'], {
			relativeTo: this.$route,
			queryParams: this.queryParams,
			queryParamsHandling: 'merge',
		}).catch(err => {
			console.error(err);
		});
	};

	showSimilarByAuthor = (authorId: number): void => {
		this.$api.search({
			sort: this.queryParams.sort,
			user: authorId,
		}).subscribe(list => {
			this.similarList = list;
		});
	};

	showSimilarByTag = (tag: string): void => {
		this.$api.search({
			sort: this.queryParams.sort,
			tagged: tag,
		}).subscribe(list => {
			this.similarList = list;
		});
	};

	closeSimilar = (): void => {
		this.similarList = undefined;
	};

	ngOnInit(): void {
	}

	ngOnDestroy(): void {
		if (this.dataSubscription.closed) {
			this.dataSubscription.unsubscribe();
		}
	}
}
