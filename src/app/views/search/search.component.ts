import { Component, HostListener, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { FadeInAnimation } from '../../../shared';

const SCROLL_WIDTH = 30;

@Component({
	selector: 'div#search',
	templateUrl: './search.component.html',
	styleUrls: ['./search.component.scss'],
	animations: [FadeInAnimation],
})
export class SearchComponent implements OnInit {
	innerHeight = 0;

	queryParams = new FormGroup({
		q: new FormControl(''),
	});

	constructor(private readonly $router: Router, @Inject(PLATFORM_ID) platformId: PlatformIds) {
		if (platformId === 'browser') {
			this.innerHeight = window.innerHeight - SCROLL_WIDTH;
		}
	}

	search = (): void => {
		const queryParams = this.queryParams.getRawValue();
		this.$router.navigate(['/search_results'], { queryParams }).catch(err => {
			console.error(err);
		});
	};

	ngOnInit(): void {
	}

	@HostListener('window:resize')
	onWindowResize(): void {
		this.innerHeight = window.innerHeight - SCROLL_WIDTH;
	}
}
