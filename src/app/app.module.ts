import { registerLocaleData } from '@angular/common';
import localeRu from '@angular/common/locales/ru';
import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { SharedModule } from '../shared/shared.module';
import { AppComponent } from './app.component';
import { ViewsModule } from './views/views.module';

registerLocaleData(localeRu);

@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserModule,
		SharedModule,
		ViewsModule,
	],
	providers: [{ provide: LOCALE_ID, useValue: 'ru' }],
})
export class AppModule {
}
