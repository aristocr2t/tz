declare global {
	function setTimeout(callback: (...args: any[]) => void, ms: number, ...args: any[]): number;
	function clearTimeout(timeoutId: number): void;

	function setInterval(callback: (...args: any[]) => void, ms: number, ...args: any[]): number;
	function clearInterval(timeoutId: number): void;

	interface Array<T> {
		first(): T | undefined;
		last(): T | undefined;
		equals(array: any[]): boolean;
	}

	interface Math {
		round(x: number, digits?: number): number;
		ceil(x: number, digits?: number): number;
		floor(x: number, digits?: number): number;
		toDegrees(alpha: number): number;
		toRadians(alpha: number): number;
	}

	function isFinite(number: unknown): number is number | string | boolean;
	function isNaN(number: unknown): boolean;

	interface NumberConstructor {
		isFinite(number: unknown): number is number;
		isInteger(number: unknown): number is number;
	}

	type PlatformIds = 'browser' | 'server';
	type ObjectId = string;
}

export {};
