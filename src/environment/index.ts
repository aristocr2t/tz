import 'zone.js/dist/zone-error';

const environment = {
	production: false,
	browserApiUrl: 'https://api.stackexchange.com',
	serverApiUrl: 'https://api.stackexchange.com',
};

export default environment;
